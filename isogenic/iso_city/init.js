window.igeConfig = {
	// This init method provides a good place to execute any code specific to the config
	init: function () {
		// Configure regular expression searches to map urls to internal paths
		this.requestMapping = [
			{name: 'engine', regEx: /^\/engine\//gi, mapTo: this.dir_engine + '/'},
			{name: 'assets', regEx: /^\/assets\//gi, mapTo: this.dir_game_assets + '/'},
			{name: 'modules', regEx: /^\/modules\//gi, mapTo: this.dir_engine_modules + '/'},
			{name: 'nodeModules', regEx: /^\/node_modules\//gi, mapTo: this.dir_node_modules + '/'},
			{name: 'client', regEx: /^\/client\//gi, mapTo: this.dir_game_client + '/'},
			{name: 'client', regEx: /^\//gi, mapTo: this.dir_game_client + '/'},
		];
	},
	
	mapUrl: function (url, name) {
		for (var i in this.requestMapping) {
			var map = this.requestMapping[i];
			if (url.match(map.regEx)) {
				if (name) {
					return [map.name, url.replace(map.regEx, map.mapTo), map.mapTo];
				} else {
					return url.replace(map.regEx, map.mapTo);
				}
			}
		}
	},
	// Root and node.js
	dir_root:'', // The absolute path where the IGE is located
	dir_node_modules:'', // The absolute path where the node modules are located
	
	// Engine
	dir_engine:'../engine', // The absolute path where the engine files are located
	dir_engine_modules:'../engine/modules', // The absolute path where modules are located
	
	// Game
	dir_game_assets: './assets',
	dir_game_client: '.',
	offline:true,
}
window.igeConfig.init();

// Define our engine and game instance variables
window.ige = null;

// Create the bootstrap instance and ask it to load our engine files
window.igeBootstrap = new IgeBootstrap(onBoot);

var mode = 0;

if (mode == 1) {
	// Initalise the bootstrap instance with the file list to load
	window.igeBootstrap.init([
		// Libraries
		igeConfig.mapUrl('/engine/lib_json'),
		igeConfig.mapUrl('/engine/lib_bison'),
		igeConfig.mapUrl('/engine/lib_stack'),
		// Engine Core Classes
		igeConfig.mapUrl('/engine/IgeConstants'),
		igeConfig.mapUrl('/engine/IgeClass'),
		igeConfig.mapUrl('/engine/IgeBase'),
		igeConfig.mapUrl('/engine/IgeEnum'),
		igeConfig.mapUrl('/engine/IgeEvents'),
		igeConfig.mapUrl('/engine/IgeCollection'),
		igeConfig.mapUrl('/engine/IgeItem'),
		// Networking classes
		igeConfig.mapUrl('/engine/IgeNetwork2'),
		igeConfig.mapUrl('/engine/IgeNetwork2_Packet'),
		igeConfig.mapUrl('/engine/IgeNetwork2_GameClient'),
		igeConfig.mapUrl('/engine/IgeNetworkItem'),
		igeConfig.mapUrl('/engine/IgeNetworkProvider_SocketIO'),
		igeConfig.mapUrl('/engine/IgeNetworkProvider_Pusher'),
		igeConfig.mapUrl('/engine/IgeNetworkProvider_Offline'),
		// Render classes
		igeConfig.mapUrl('/engine/IgeCanvas'),
		igeConfig.mapUrl('/engine/IgeHtml'),
		// Engine Game Classes
		igeConfig.mapUrl('/engine/IgeTemplates'),
		igeConfig.mapUrl('/engine/IgeAnimations'),
		igeConfig.mapUrl('/engine/IgePaths'),
		igeConfig.mapUrl('/engine/IgeUsers'),
		igeConfig.mapUrl('/engine/IgeAssets'),
		igeConfig.mapUrl('/engine/IgeEntities'),
		igeConfig.mapUrl('/engine/IgeScreens'),
		igeConfig.mapUrl('/engine/IgeDirtyRects'),
		igeConfig.mapUrl('/engine/IgeMaps'),
		igeConfig.mapUrl('/engine/IgeCameras'),
		igeConfig.mapUrl('/engine/IgeViewports'),
		igeConfig.mapUrl('/engine/IgeBackgrounds'),
		igeConfig.mapUrl('/engine/IgeRenderer'),
		igeConfig.mapUrl('/engine/IgeTime'),
		igeConfig.mapUrl('/engine/IgeIdFactory'),
		igeConfig.mapUrl('/engine/IgePallete'),
		igeConfig.mapUrl('/engine/IgeWindow'),
		igeConfig.mapUrl('/engine/IgeEngine'),
		igeConfig.mapUrl('/client/index'),
	]);
} else {
	window.igeBootstrap.init([
		igeConfig.mapUrl('/engine/_deploy'),
		igeConfig.mapUrl('/client/index'),
	]);
}

setTimeout(function () {
	console.log('-----------------------------------------------------');
	console.log('Isogenic Game Engine - http://www.isogenicengine.com');
	console.log('(C)opyright 2011 Irrelon Software Limited');
	console.log('-----------------------------------------------------');
	window.igeBootstrap.process.apply(window.igeBootstrap);
}, 200);

function onBoot () {
	$(document).ready(function () {
		Init = new IgeClass({
			init: function () {
				this._className = 'Init';
				this.log('jQuery says DOM is ready');
				
				// Setup engine instance
				this.log('Setting up engine instance');
				window.ige = new IgeEngine();
				window.ige.defaultContext = '2d'; // Set default canvas context
				
				// Create the server script instance and pass the ige as the engine
				window.server = {}; //new this.sandbox.GameServer(ige);
				// Create the client script instance and pass the ige as the engine
				window.client = new GameClient(ige);
				// Create the game script instance and pass the client and server instance variables
				window.game = new Game(window.ige, window.client, window.server);
				
				window.ige.on('networkProviderReady', this.bind(function () {
					// Setup engine hooks
					this.log('Setting up engine hooks');
					safeCall(window.game, 'engineHooks');
					
					// Register the game network commands
					this.log('Registering network commands');
					safeCall(window.game, 'networkCommands');
					
					// Connect to the server
					this.log('Setting engine as ready');
					safeCall(window.game, 'ready');
				}), null, true);
				
				// Setup network
				this.log('Setting up network provider');
				safeCall(window.game, 'networkInit');
			}
		});
		init = new Init();
	});
}