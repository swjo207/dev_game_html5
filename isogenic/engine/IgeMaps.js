/** IgeMaps - The map management class. {
	engine_ver:"0.0.6",
	category:"class",
} **/

/** beforeCreate - Fired before a map is created. {
	category: "event",
} **/
/** afterCreate - Fired after a map is created. {
	category: "event",
} **/
IgeMaps = new IgeClass({
	
	Extends: [IgeCollection, IgeItem, IgeEvents],
	
	/** engine - A reference object to the main engine instance. {
		category:"property",
		type:"object",
		instanceOf:"IgeEngine",
	} **/
	engine: null,
	
	/** entities - A reference to this.engine.entities for fast access. {
		category:"property",
		type:"object",
		instanceOf:"IgeEntities",
	} **/
	entities: null,
	
	/** byIndex - An array with an integer index that allows you to access every
	item created using the create method in the order it was created. {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	byIndex: null,
	
	/** byId - An array with a string index that allows you to access every item
	created using the create method by its id. {
		category:"property",
		type:"array",
		index:"string",
	} **/
	byId: null,
	
	/** tileMap - An array with a string index that allows you to access arbitrary
	marks on the tile map by their x, y and type (tileMap[map_id][x][y][type]). {
		category:"property",
		type:"array",
		index:"string",
	} **/
	tileMap: null,
	
	/** init - The constructor for this class. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a new instance of this class",
		},
		argument: {
			type:"object",
			name:"engine",
			desc:"The active engine instance.",
			instanceOf:"IgeEngine",
		},
	} **/
	init: function (engine) {
		this._className = 'IgeMaps';
		this.engine = engine;
		this.collectionId = 'map';
		this.entities = this.engine.entities.byMapId;
		
		this.byIndex = [];
		this.byId = [];
		this.tileMap = [];
		
		// Check if we have a network
		if (this.engine._IgeNetwork) {
			// Call the networkInit method of this class
			this.networkInit();
		}			
	},
	
	networkInit: function () {
		// Network CRUD Commands
		this.engine.network.registerCommand('mapsCreate', this.bind(this.receiveCreate));
		this.engine.network.registerCommand('mapsUpdate', this.bind(this.receiveUpdate));
		this.engine.network.registerCommand('mapsRemove', this.bind(this.receiveRemove));
		
		// Register standard property names to the network manifest see the API manual for more details
		this.engine.network.addToManifest([
			'map_id',
		]);
	},
	
	/** create - Create a new map. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns the newly created map or false on failure.",
		},
		arguments: [{
			type:"object",
			name:"map",
			desc:"The map object to be created.",
			link:"mapData",
		}, {
			type:"method",
			name:"callback",
			desc:"The method to call when the map has been created successfully.",
		}],
	} **/
	create: function (map) {
		
		// Check if we need to auto-generate an id
		this.ensureIdExists(map);
		
		// Check that this entity does not already exist
		if (!this.byId[map.map_id]) {
			
			this.emit('beforeCreate', map);
			
			// Create the local storage object
			map.$local = map.$local || {};
			
			// Load any template properties
			if (map.template_id) { this.engine.templates.applyTemplate(map, map.template_id); }
			
			// If we're a client, do some stuff here
			if (!this.engine.isServer) {
				// Based upon the current browser, set the layer type (canvas, html, webgl) if the layer is set to auto
				var mapLayers = map.map_layers;
				
				if (mapLayers) {
					for (var layerIndex = 0; layerIndex < mapLayers.length; layerIndex++) {
						var layer = mapLayers[layerIndex];
						
						// Check if the layer is set to auto-detect the best output type
						if (layer.layer_type == LAYER_TYPE_AUTO) {
							// Default to canvas output
							layer.layer_type = LAYER_TYPE_CANVAS;
						}
					}
				} else {
					this.log('Unable to create map because the map object does not contain a map_layers array!', 'error', map);
				}
			}
			
			map.map_tilesize2 = map.map_tilesize / 2;
			map.map_tilesize4 = map.map_tilesize / 4;
			
			// If the map is set to use dirty rectangles, set them up!
			if (map.map_use_dirty) {
				if (map.map_dirty_width && map.map_dirty_height) {
					this.engine.dirtyRects.create(map, map.map_dirty_width, map.map_dirty_height);
				} else {
					this.log('Cannot setup map dirty rectangle system because either no width or no height was specified.', 'error', map);
				}
			}
			
			// Push the map to the collection
			this.byIndex.push(map);
			this.byId[map.map_id] = map;
			
			this.emit('afterCreate', map);			
			
			return map;
			
		} else {
			// The entity already exists so ignore this request
			this.log('Attempted to create a map that already exists with id: ' + map[this.collectionId + '_id'], 'info');
			return false;
		}
				
	},
	
	update: function () {},
	remove: function () {},
	
	/** blockTile - Sets a tile as blocked so that entities cannot be placed there. {
		category:"method",
		arguments: [{
			type:"string",
			name:"mapId",
			desc:"The id of the map to set the block on.",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate of the tile to block.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate of the tile to block.",
		}],
	} **/
	blockTile: function (mapId, x, y) {
		this.markTile(mapId, x, y, true, 'igeBlock');
	},
	
	/** unBlockTile - Removes a tile blocking flag so that entities can be placed there. {
		category:"method",
		arguments: [{
			type:"string",
			name:"mapId",
			desc:"The id of the map to clear the block on.",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate of the tile to un-block.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate of the tile to un-block.",
		}],
	} **/
	unBlockTile: function (mapId, x, y) {
		this.markTile(mapId, x, y, false, 'igeBlock');
	},
	
	/** isTileBlocked - Check if a tile has been blocked. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true if the tile is blocked, false if not.",
		},
		arguments: [{
			type:"string",
			name:"mapId",
			desc:"The id of the map to check the block on.",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate of the tile to check.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate of the tile to check.",
		}],
	} **/
	isTileBlocked: function (mapId, x, y) {
		if (this.tileMap[mapId] && this.tileMap[mapId][x] && this.tileMap[mapId][x][y] && this.tileMap[mapId][x][y]['igeBlock']) {
			return true;
		} else {
			return false;
		}
	},
	
	/** markTile - Marks a tile with the passed data. {
		category:"method",
		arguments: [{
			type:"string",
			name:"mapId",
			desc:"The id of the map to mark the tile on.",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate of the tile to mark.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate of the tile to mark.",
		}, {
			type:"multi",
			name:"mark",
			desc:"The data to mark the tile with. This can be any valid JavaScript data type except types that evaluate to false.",
		}, {
			type:"string",
			name:"type",
			desc:"A string denoting the type of mark to store. 'igeBlock' is used for tile blocking, you may use any other value.",
		}],
	} **/
	markTile: function (mapId, x, y, mark, type) {
		if (!type) { type = 0; } // Set a default type to avoid crashing
		this.tileMap[mapId] = this.tileMap[mapId] || [];
		this.tileMap[mapId][x] = this.tileMap[mapId][x] || [];
		this.tileMap[mapId][x][y] = this.tileMap[mapId][x][y] || [];
		this.tileMap[mapId][x][y][type] = mark;
	},
	
	/** unMarkTile - Remove all marking data from a tile. {
		category:"method",
		arguments: [{
			type:"string",
			name:"mapId",
			desc:"The id of the map to remove marking data from.",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate of the tile to remove marking data from.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate of the tile to remove marking data from.",
		}, {
			type:"string",
			name:"type",
			desc:"A string denoting the type of mark to un-mark. 'igeBlock' is used for tile blocking, you may use any other value.",
		}],
	} **/
	unMarkTile: function (mapId, x, y, type) {
		if (!type) { type = 0; } // Set a default type to avoid crashing
		if (this.tileMap[mapId] && this.tileMap[mapId][x] && this.tileMap[mapId][x][y] && this.tileMap[mapId][x][y][type]) {
			this.tileMap[mapId][x][y][type] = null;
		}
	},
	
	/** getTileMark - Get the marking data on a tile. {
		category:"method",
		return: {
			type:"multi",
			desc:"Returns the marking data applied to a tile, or null if the tile has no data.",
		},
		arguments: [{
			type:"string",
			name:"mapId",
			desc:"The id of the map to return marking data from.",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate of the tile to return marking data from.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate of the tile to return marking data from.",
		}, {
			type:"string",
			name:"type",
			desc:"A string denoting the type of mark to lookup. 'igeBlock' is used for tile blocking, you may use any other value.",
		}],
	} **/	
	getTileMark: function (mapId, x, y, type) {
		if (!type) { type = 0; } // Set a default type to avoid crashing
		if (this.tileMap[mapId] && this.tileMap[mapId][x] && this.tileMap[mapId][x][y]) {
			return this.tileMap[mapId][x][y][type];
		} else {
			return null;
		}
	},
	
	makeMapDirty: function (map) {
		var curMap = this.read(map);
		if (curMap && curMap.map_id) {
			var viewports = this.engine.viewports.byMapId[curMap.map_id];
			if (viewports && viewports.length) {
				for (var i = 0; i < viewports.length; i++) {
					this.engine.viewports.makeViewportDirty(viewports[i]);
				}
			}
		}
	},
	
	toggleOption: function (map, option) {
		if (map) {
			// Toggle the map option
			if (option == 'map_use_dirty' && !map.map_use_dirty && typeof(map.$local.$dirtyRects) == 'undefined') {
				this.log('Cannot enabled dirty rectangle-based rendering because the game was not started with it enabled!', 'warning');
				return false;
			}
			if (map[option] == true) {
				map[option] = false;
			} else {
				map[option] = true;
			}
			
			// Set the map to dirty
			this.makeMapDirty(map);
		}
	}
	
});