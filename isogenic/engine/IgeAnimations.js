/** IgeAnimations - The animations manager. {
	engine_ver:"0.1.0",
	category:"class",
} **/

/** beforeCreate - Fired before an animation is created. {
	category: "event",
	argument: {
		type:"object",
		name:"animation",
		desc:"The animation object this event was fired for.",
	},
} **/
/** afterCreate - Fired after an animation is created. {
	category: "event",
	argument: {
		type:"object",
		name:"animation",
		desc:"The animation object this event was fired for.",
	},
} **/
IgeAnimations = new IgeClass({
	
	Extends: [IgeCollection, IgeItem, IgeEvents],
	
	/** engine - A reference object to the main engine instance. {
		category:"property",
		type:"object",
		instanceOf:"IgeEngine",
	} **/
	engine: null,
	
	/** entities - A reference to this.engine.entities. {
		category:"property",
		type:"object",
		instanceOf:"IgeEntities",
	} **/
	entities: null,
	
	/** byIndex - An array with an integer index that allows you to access every
	item created using the create method in the order it was created. {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	byIndex: null,
	
	/** byId - An array with a string index that allows you to access every item
	created using the create method by its id. {
		category:"property",
		type:"array",
		index:"string",
	} **/
	byId: null,
	
	/** init - The constructor for this class. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a new instance of this class",
		},
		argument: {
			type:"object",
			name:"engine",
			desc:"The active engine instance.",
			instanceOf:"IgeEngine",
		},
	} **/
	init: function (engine) {
		this._className = 'IgeAnimation';
		
		this.engine = engine;
		this.entities = this.engine.entities;
		
		this.byIndex = [];
		this.byId = [];
		
		this.collectionId = 'animation';
		
		// Check if we have a network
		if (this.engine._IgeNetwork) {
			// Call the networkInit method of this class
			this.networkInit();
		}
	},
	
	networkInit: function () {
		// Network CRUD Commands
		this.engine.network.registerCommand('animationsCreate', this.bind(this.receiveCreate));
		this.engine.network.registerCommand('animationsUpdate', this.bind(this.receiveUpdate));
		this.engine.network.registerCommand('animationsRemove', this.bind(this.receiveRemove));
		
		// Register standard property names to the network manifest see the API manual for more details
		this.engine.network.addToManifest([
			'animation_dirty',
			'animation_id',
			'animation_frame_from',
			'animation_frame_to',
			'animation_fps',
			'animation_loop',
		]);
	},
	
	/** create - Create a new animation to be used for entity animation. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true on success or false on failure.",
		},
		argument: {
			type:"object",
			name:"animation",
			desc:"The animation object to be created.",
			link:"animationData",
		},
	} **/
	create: function (animation) {
		if (!this.entities) { this.entities = this.engine.entities; }
		
		// Check if we need to auto-generate an id
		this.ensureIdExists(animation);
			
		if (animation.animation_fps) { 
			this.emit('beforeCreate', animation);
			// Set local vars
			animation.$local = {};
			animation.$local.$frameCount = this.getFrameCount(animation);
			//animation.$local.$fpsTime = Math.floor(1000 / animation.animation_fps);
			animation.$local.$frameTime = Math.floor(1000 / animation.animation_fps);
			//animation.$local.$frameTime = animation.animation_fps / animation.$local.$frameCount;
			
			// Set defaults
			if (animation.animation_loop == null) { animation.animation_loop = true; } // Default is to loop animations
			
			this.byIndex.push(animation);
			this.byId[animation.animation_id] = animation;
			
			return animation;
			
			this.emit('afterCreate', animation);
			
			return true;
		} else {
			this.log('Cannot create animation because it has no fps setting (animation_fps)!', 'error', animation);
			return false;
		}
		
	},
	
	update: function () {},
	remove: function () {},
	
	/** animateByDelta - Use the delta time to set the correct asset_sheet_frame for the passed entity. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true if the entity animation frame has changed or false if not.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity object that is to be animated by modifying its asset_sheet_frame.",
		}, {
			type:"integer",
			name:"delta",
			desc:"The amount of time that has passed between the last time the animateByDelta was called and the present; usually the value of this.engine.currentDelta.",
		}],
	} **/
	animateByDelta: function (entity, delta) {
		if (delta) {
			var animation = entity.$local.$animation;
			
			// Advance the entity internal animation time by the delta
			this.advanceTimeByDelta(entity, delta);
			
			// Get the new frame and compare it to the current
			var newFrame = this.getFrame(entity);
			if (newFrame != null && !isNaN(newFrame)) {
				if (newFrame != entity.asset_sheet_frame) {
					// The new frame is different from the current so update
					this.entities.setSheetFrame(entity, newFrame);
					//entity.asset_sheet_frame = newFrame;
					
					// Check if we should mark this entity as dirty when animation frames change
					if (entity.animation_dirty) {
						this.engine.entities.markDirty(entity);
					}
					
					return true;
				}
			} else {
				this.log('Entity sheet frame error', 'error', entity);
				return false;
			}
		} else {
			return false;
		}
	},
	
	/** advanceTimeByDelta - Advances the internal animation time by the delta time passed.
	The internal animation time of an entity is used when determining the current
	animation frame index. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true at all times.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity whose internal animation time is to be advanced.",
		}, {
			type:"integer",
			name:"delta",
			desc:"The delta time to add to the internal animation time of the entity. This can be a negative value to make the animation run backwards.",
		}],
	} **/
	advanceTimeByDelta: function (entity, delta) {
		entity.$local.$animation_time += delta;
		
		// Check that the time is within a second's bounds
		var secCheck = entity.$local.$animation_time / 1000;
		if (Math.abs(secCheck) >= 1) {
			entity.$local.$animation_time -= (1000 * Math.floor(secCheck));
		}
		
		if (entity.$local.$animation_time < 0) { entity.$local.$animation_time = 1000 + entity.$local.$animation_time; }
		
		return true;
	},
	
	/** getFrame - Returns the current animation frame index of the entity specified. {
		category:"method",
		return: {
			type:"integer",
			desc:"Returns the current animation frame index or false on error. Animation frame indexes start at 1, not 0.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity whose animation frame you want to return.",
		}],
	} **/
	getFrame: function (entity) {
		if (entity && entity.$local && entity.$local.$animation && entity.$local.$animation.$local) {
			var entityLocal = entity.$local;
			var entityAnimation = entity.$local.$animation;
			var animationLocal = entityAnimation.$local;
			
			var internalFrame = Math.floor(entityLocal.$animation_time / animationLocal.$frameTime);
			if(internalFrame + 1 > animationLocal.$frameCount)
			{
				var multiple = Math.floor(internalFrame / animationLocal.$frameCount);
				internalFrame -= (animationLocal.$frameCount * multiple);
			}
			
			if (entityAnimation.animation_frame_list) {
				 var finalIndex = entityAnimation.animation_frame_list[internalFrame];
			} else if (entityAnimation.animation_frame_from && entityAnimation.animation_frame_to) {
				var finalIndex = entityAnimation.animation_frame_from + internalFrame;
				//console.log(internalFrame, finalIndex, entity.$local.$animation_time);
				if (finalIndex < entityAnimation.animation_frame_from)
				{
					//console.log(internalFrame, finalIndex, entityLocal.$animation_time, animationLocal.$frameTime);
					throw("Animation error lower");
				}
				if (finalIndex > entityAnimation.animation_frame_to)
				{
					//console.log(internalFrame, finalIndex, entityLocal.$animation_time, animationLocal.$frameTime);
					throw("Animation error higher");
				}
			}
			
			return finalIndex;
		} else {
			return 0;
		}
	},
	
	/** getFrameCount - Returns the number of frames in the animation. {
		category:"method",
		return: {
			type:"integer",
			desc:"Returns the number of frames in the animation. If an animation_frame_list is not set or both animation_frame_from and animation_frame_to are not set this method will throw an error.",
		},
		arguments: [{
			type:"object",
			name:"animation",
			desc:"The animation object whose frame count is to be returned.",
		}],
	} **/
	getFrameCount: function (animation) {
		if (animation.animation_frame_list) {
			return animation.animation_frame_list.length;
		} else if (animation.animation_frame_from && animation.animation_frame_to) {
			return Math.abs(animation.animation_frame_to - animation.animation_frame_from) + 1;
		} else {
			this.log('Cannot return number of frames from animation "' + animation.animation_id + '" because neither a frame array (animation_frame_list) or frame start and end (animation_frame_from, animation_frame_to) are defined.', 'error');
			return false;
		}
	},
	
});