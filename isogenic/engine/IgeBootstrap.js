// Create bootstrap class
IgeBootstrap = function (callback, supressDefault) {
	this.onComplete = callback;
	this.serverFiles = null;
	this.clientFiles = null;
	this.busy = false;
	
	if (!supressDefault) {
		
		if (typeof module !== 'undefined' && module.exports) {
			this.mode = 0;
		} else if (window) {
			// Make sure that we don't cause issues trying to use the console on crappy browsers
			if (window != null && !window.console) {
				window.console = {
					log: function () {}
				};
			}
			this.mode = 1;
		} else {
			throw('Bootstrap error, cannot detect environment!');
		}
	}
};

IgeBootstrap.prototype.init = function (fileList) {
	if (this.mode == 0) {
		this.serverFiles = fileList;
		for (var i in this.serverFiles) { this.require(this.serverFiles[i]); }
	}
	
	if (this.mode == 1) {
		this.clientFiles = fileList;
		for (var i in this.clientFiles) { this.require(this.clientFiles[i]); }
	}	
}

// Define the boot loader class
IgeBootstrap.prototype.require = function (file, varName, callback) {
	
	// Check that we were passed a filename
	if (file) {
		// Check the load queues exist
		this.queue = this.queue || [];
		this.done = this.done || [];
		this.doneCount = 0;
		
		// Add the script file to the load queue
		this.queue.push([file, varName, callback]);
		//this.log('IGE *info* [IgeBootstrap] : Added to queue: ' + file);
	}	
	
}

IgeBootstrap.prototype.process = function () {
	
	if (!this.busy) {
		
		this.busy = true;
		
		if (this.queue.length) {
			
			this.loadFile(this.queue.shift());
			
		} else {
			
			// Set busy to false
			this.busy = false;
			
			// Loading files is complete so fire the complete function
			if (typeof this.onComplete == 'function') {
				if (this.mode == 0) {
					// Server
					this.onComplete.call();
				}
				
				if (this.mode == 1) {
					this.onComplete.apply(window);
				}
				
				// Clear the onComplete callback because we've called it
				this.onComplete = null;
			}
			
		}
		
	} else {
		this.log('IGE *info* [IgeBootstrap] : Queue busy');
	}
	
}

IgeBootstrap.prototype.loadFile = function (fileData) {
	
	var file = fileData[0];
	var varName = fileData[1];
	var callback = fileData[2];
	
	// Check if we have already included this file in the DOM
	if (this.done[file]) {
		this.log('IGE *info* [IgeBootstrap] : Already loaded file ' + file);
		if (this.mode == 0) {
			this.busy = false;
			this.process();
		}
		if (this.mode == 1) {
			window.igeBootstrap.busy = false;
			window.igeBootstrap.process();
		}
		return;
	}
	
	//this.log('IGE *info* [IgeBootstrap] : Loading file ' + file);
	this.doneCount++;
	
	if (this.mode == 0) {
		// Server
		if (varName) {
			eval(varName + ' = require("' + file + '");');
			if (typeof callback == 'function') { callback.apply(this, fileData); }
		} else {
			require(file); // Node.js require call
		}
		
		this.busy = false;
		this.process();
	}
	
	if (this.mode == 1) {
		// Client
		var tempScript = document.createElement('script');
		tempScript.onload = this.bind(function () {
			this.log('IGE *info* [IgeBootstrap] : File loaded: ' + file);
			// Set busy to false
			window.igeBootstrap.busy = false;
			// Call the callback to inform it that the file has loaded
			if (typeof callback == 'function') { callback.apply(this, [fileData]); }
			// Execute the process method to process the next in queue
			window.igeBootstrap.process();
		});
		
		tempScript.id = 'IgeBootstrap_' + this.doneCount + new Date().getTime();
		tempScript.type = 'text/javascript';
		tempScript.src = file + '.js';
		
		document.getElementsByTagName("head")[0].appendChild(tempScript);
	}
	
	this.done[file] = true;
	
}

IgeBootstrap.prototype.bind = function( Method ) {
	
	if (typeof Method == 'function') {
		var _this = this;
		return(
			function(){
				return( Method.apply( _this, arguments ) );
			}
		);
	} else {
		this.log('IGE *info* [IgeBootstrap] : An attempt to use bind against a method that does not exist was made!');
		return (function () { });
	}
	
}

IgeBootstrap.prototype.log = function (msg) {
	// Server code
	if (this.mode == 0) {
		console.log(msg);
	}
	
	// Client code
	if (this.mode == 1) {
		if (window.igeDebug) {
			if (console != null && console.log != null) {
				console.log(msg);
			}
		}
	}
}

safeCall = function (obj, methodName, args) {
	if (obj && typeof obj[methodName] == 'function') {
		obj[methodName].apply(obj, args || []);
		return true;
	} else {
		// No method
		//this.log('Cannot call method "' + methodName + '" of passed object (object output above).', 'warning', obj);
		return false;
	}
}

// Define console if it does not already exist
if (typeof(console) == 'undefined') {
	var emptyFunc = function () {}
	console = {};
	
	console.log = emptyFunc;
	console.info = emptyFunc;
	console.warn = emptyFunc;
	console.error = emptyFunc;
	
	if (typeof(window) != 'undefined' && typeof(window.console) == 'undefined') { window.console = console; }
}

// Define some basic engine variables as null
IgeCollection = null;
IgeNetworkItem = null;