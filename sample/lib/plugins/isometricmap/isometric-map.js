ig.module(
    'plugins.isometricmap.isometric-map'
)
.requires(
    'impact.impact',
    'impact.background-map'
)
.defines(function(){

ig.IsometricMap = ig.BackgroundMap.extend({

     init: function( tilesize, data, tileset, offsetX, offsetY ) {
      this.parent(tilesize, data, tileset);
      this.offsetX = offsetX;
      this.offsetY = offsetY;
    },

    draw: function() {
      if( !this.tiles.loaded || !this.enabled ) {
          return;
      }      
      this.drawTiled();

    },
    drawTiled: function() {
      var tile = 0,
        anim = null,
        //tileOffsetX = (this.scroll.x / this.tilesize).toInt(),
        //tileOffsetY = (this.scroll.y / this.tilesize).toInt(),
        tileOffsetX = 0,
        tileOffsetY = 0,
        pxOffsetX = this.scroll.x % this.tilesize,
        pxOffsetY = this.scroll.y % this.tilesize,
        pxMinX = -pxOffsetX - this.tilesize + this.offsetX,
        pxMinY = -pxOffsetY - this.tilesize + this.offsetY,
        pxMaxX = ig.system.width - this.tilesize - pxOffsetX + this.offsetX + this.scroll.x,
        pxMaxY = ig.system.height - this.tilesize - pxOffsetY + this.offsetY+this.scroll.y,
        iso_pxX = 0,
        iso_pxY = 0;
        var offset = {x:-ig.system.getDrawPos(this.scroll.x) - 0.5 + this.offsetx,
				y:-ig.system.getDrawPos(this.scroll.y) - 0.5 + this.offsety
		};
        console.log (tileOffsetX+','+tileOffsetY+'['+pxMinX+','+pxMinY+','+pxMaxX+','+pxMaxY);
        var log = ''; 
        for( var mapY = 0; mapY < this.height; mapY++) {
            var tileY = mapY + tileOffsetY;

            // Repeat Y?
            //if( tileY >= this.height || tileY < 0 ) {
            //    if( !this.repeat ) { continue; }  
            //    tileY = tileY > 0
            //        ? tileY % this.height
            //        : ((tileY+1) % this.height) + this.height - 1;
            //}
        
            //for( var mapX = -1, pxX = pxMinX; pxX < pxMaxX; mapX++, pxX += this.tilesize ) {
            for( var mapX = 0; mapX < this.width; mapX++ ) {
                var tileX = mapX + tileOffsetX;
            
                // Repeat X?
                //if( tileX >= this.width || tileX < 0 ) {
                //    if( !this.repeat ) { continue; }
                //    tileX = tileX > 0
                //        ? tileX % this.width
                //        : ((tileX+1) % this.width) + this.width - 1;
                //}
                
                // Draw!
                if( (tile = this.data[tileY][tileX]) ) {
                    iso_pxX = ((this.tilesize/2)*tileX) - ((this.tilesize/2)*tileY)+ offset.x ;
                    iso_pxY = ((this.tilesize/4)*tileY) + ((this.tilesize/4)*tileX)+ offset.y ;
                log+= '('+iso_pxX+','+iso_pxY+')';      
                    this.tiles.drawTile(iso_pxX ,
                                        iso_pxY , tile-1, this.tilesize );
    
                }
            } // end for x
        } // end for y
        console.log(log);
    }
  });

});