ig.module(
	'plugins.ios.input'
)
.requires(
	'impact.input'
)
.defines(function(){


ig.Input.inject({
	_touchInput: null,
	_nativeButtons: [],
	
	initMouse: function() {
		if( this.isUsingMouse ) { return; }
		this.isUsingMouse = true;
		this._touchInput = new native.TouchInput();
		
		var that = this;
		this._touchInput.touchStart( this.touchStart.bind(this) );
		this._touchInput.touchEnd( this.touchEnd.bind(this) );
		this._touchInput.touchMove( this.touchMove.bind(this) );
	},
	
	touchStart: function( x, y ) {
		this.mouse.x = x / ig.system.scale;
		this.mouse.y = y / ig.system.scale;
		
		var action = this.bindings[ig.KEY.MOUSE1];
		if( action ) {
			this.actions[action] = true;
			this.presses[action] = true;
		}
	},
	
	touchEnd: function( x, y ) {
		this.mouse.x = x / ig.system.scale;
		this.mouse.y = y / ig.system.scale;
		
		var action = this.bindings[ig.KEY.MOUSE1];
		if( action ) {
			this.delayedKeyup[action] = true;
		}
	},
	
	touchMove: function( x, y ) {
		this.mouse.x = x / ig.system.scale;
		this.mouse.y = y / ig.system.scale;
	},
	
	
	bindTouchArea: function( x, y, w, h, action ) {
		var that = this;
		var button = new native.Button( x, y, w, h, 
			function() {
				that.actions[action] = true;
				that.presses[action] = true;
			},
			function() {
				that.delayedKeyup[action] = true;
			}
		);
		this._nativeButtons.push( button );
		return button;
	},
	
	unbindAll: function() {
		this._nativeButtons = [];
	}
});

});