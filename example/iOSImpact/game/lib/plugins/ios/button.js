ig.module(
	'plugins.ios.button'
)
.requires(
	'impact.input'
)
.defines(function(){

ig.Input.inject({
	_nativeButtons: [],
	
	bindTouchArea: function( x, y, w, h, action ) {
		var that = this;
		var button = new native.Button( x, y, w, h, 
			function() {
				that.actions[action] = true;
				that.presses[action] = true;
			},
			function() {
				that.delayedKeyup.push( action );
			}
		);
		this._nativeButtons.push( button );
		return button;
	},
	
	unbindAll: function() {
		this._nativeButtons = [];
	}
});


});