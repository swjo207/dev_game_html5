ig.module(
	'plugins.ios.animation'
)
.requires(
	'impact.animation'
)
.defines(function(){


ig.Animation.inject({
	draw: function( targetX, targetY ) {
		var bbsize = Math.max(this.sheet.width, this.sheet.height);
		
		// On screen?
		if(
		   targetX > ig.system.width || targetY > ig.system.height ||
		   targetX + bbsize < 0 || targetY + bbsize < 0
		) {
			return;
		}
		
		if( this.angle == 0 ) {
			ig.system.context.drawImageTile( 
				this.sheet.image.data, 
				targetX, targetY, 
				this.tile,
				this.sheet.width, this.sheet.height, 
				this.flip.x, this.flip.y, this.alpha 
			);
		}
		else {
			ig.system.context.save();
			ig.system.context.translate(
				ig.system.getDrawPos(targetX + this.pivot.x),
				ig.system.getDrawPos(targetY + this.pivot.y)
			);
			ig.system.context.rotate( this.angle );
			this.sheet.image.drawTile(
				-this.pivot.x, -this.pivot.y,
				this.tile, this.sheet.width, this.sheet.height,
				this.flip.x, this.flip.y
			);
			ig.system.context.restore();
		}
	}
});	


});