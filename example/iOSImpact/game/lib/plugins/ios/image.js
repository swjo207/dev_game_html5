ig.module(
	'plugins.ios.image'
)
.requires(
	'impact.system',
	'impact.image',
	'impact.font'
)
.defines(function(){


ig.Image.inject({
	resize: function(){},
	reload: function(){},
	
	load: function( loadCallback ) {
		if( this.loaded ) {
			if( loadCallback ) {
				loadCallback( this.path, true );
			}
			return;
		}
		else if( !this.loaded && ig.ready ) {
			this.loadCallback = loadCallback || null;
			this.data = new native.Texture( this.path, this.onload.bind(this) );
		}
		else {
			ig.addResource( this );
		}
		ig.Image.cache[this.path] = this;
	},

	onload: function( width, height ) {
		this.width = width;
		this.height = height;
		this.loaded = true;
		
		if( this.loadCallback ) {
			this.loadCallback( this.path, true );
		}
	},
	
	free: function() {
		this.data.free();
		delete ig.Image.cache[this.path];
	},

	draw: function( x, y ) {
		if( !this.loaded ) { return; }
		ig.system.context.drawImage( this.data, x, y );
	},
	
	drawTile: function( x, y, t, w, h, flipX, flipY ) {
		if( !this.loaded ) { return; }
		ig.system.context.drawImageTile( this.data, x, y, t, w, (h||w), !!flipX, !!flipY );
	}
});


});