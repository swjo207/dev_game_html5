ig.module(
	'plugins.ios.ios'
)
.requires(
	'plugins.ios.animation',
	'plugins.ios.input',
	'plugins.ios.font',
	'plugins.ios.image',
	'plugins.ios.loader',
	'plugins.ios.system'
)
.defines(function(){

/* empty module to require all iOS plugin modules */

});