#import <Foundation/Foundation.h>
#import "JS_BaseClass.h"

#import "Texture.h"
#import "Font.h"
#import "Drawable.h"

#import "EAGLView.h"


#define CANVAS_CONTEXT_BUFFER_SIZE 128

typedef union tag_gl_color { 
	struct tag_color { 
		GLubyte r,g,b,a;
	} rgba;
	GLuint hex; 
} gl_color;

gl_color JSValueToColor(JSContextRef ctx, JSValueRef value);


@interface JS_Canvas : JS_BaseClass <Drawable> {
	int width;
	int height;
	float scale;
	
	Texture * texture;
	Texture * currentTexture;
	
	int quadIndex;
	int drawCalls;
	
	gl_color fillColor;
	float clearColor[4];
	GLubyte globalAlpha;
	
	GLfloat quadVertices[CANVAS_CONTEXT_BUFFER_SIZE][12];
	GLfloat textureVertices[CANVAS_CONTEXT_BUFFER_SIZE][12];
	GLubyte vertexColors[CANVAS_CONTEXT_BUFFER_SIZE][24];
}

- (void)setFrameBuffer;
- (void)prepareThisCanvas;
- (void)switchToThisCanvas;
- (void)flushBuffers;
- (void)setCurrentTextureFromDrawable:(NSObject<Drawable> *)drawable;
- (void)drawQuadWithColor:(GLuint)color
	sx:(float)sx sy:(float)sy sw:(float)sw sh:(float)sh
	dx:(float)dx dy:(float)dy dw:(float)dw dh:(float)dh
	flipX:(BOOL)flipX flipY:(float)flipY;
	
@property (readonly) Texture * texture;

@end
