#import <Foundation/Foundation.h>
#import "JS_Canvas.h"

@interface JS_ScreenCanvas : JS_Canvas {
	EAGLView * glview;
	
	BOOL landscapeMode;
	UIDeviceOrientation orientation;
}

- (void)orientationChange:(NSNotification *)notification;

@end
