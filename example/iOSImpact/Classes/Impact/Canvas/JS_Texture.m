#import "JS_Texture.h"


@implementation JS_Texture

@synthesize texture;

- (id)initWithContext:(JSContextRef)ctx object:(JSObjectRef)obj argc:(size_t)argc argv:(const JSValueRef [])argv {
	if( self  = [super initWithContext:ctx object:obj argc:argc argv:argv] ) {
		path = [JSValueToNSString( ctx, argv[0] ) retain];
		texture = [[Texture alloc] initWithPath:[Impact pathForResource:path]];
		
		NSLog(@"Loading Image: %@", path );
		
		if( texture.textureId ) {
			JSObjectRef func = JSValueToObject(ctx, argv[1], NULL);
			JSValueRef params[] = {
				JSValueMakeNumber(ctx, texture.width),
				JSValueMakeNumber(ctx, texture.height)
			};
			[[Impact instance] invokeCallback:func thisObject:NULL argc:2 argv:params];
		}
	}
	return self;
}


JS_FUNC(JS_Texture, free, ctx, argc, argv) {
	NSLog(@"Freeing: %@", path );
	[texture release];
	texture = nil;
	return NULL;
}


- (void)dealloc {
	if( texture ) {
		[texture release];
		texture = nil;
	}
	[path release];
	[super dealloc];
}

@end
