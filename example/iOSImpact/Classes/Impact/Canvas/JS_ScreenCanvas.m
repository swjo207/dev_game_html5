#import "JS_ScreenCanvas.h"


@implementation JS_ScreenCanvas

extern EAGLContext * CanvasGlobalGLContext;
extern JS_Canvas * CanvasCurrentInstance;

- (id)initWithContext:(JSContextRef)ctx object:(JSObjectRef)obj argc:(size_t)argc argv:(const JSValueRef [])argv {
	if( self  = [super initWithContext:ctx object:obj argc:argc argv:argv] ) {
		width = JSValueToNumberFast(ctx, argv[0]);
		height = JSValueToNumberFast(ctx, argv[1]);
		scale = JSValueToNumberFast(ctx, argv[2]);
		
		NSLog(@"CanvasScreenContext: init %d, %d, %f", width, height, scale);
		
		float viewX = 0, 
			viewY = 0, 
			viewWidth = width, 
			viewHeight = height;
		
		if( [Impact landscapeMode] ) {
			viewWidth = height;
			viewHeight = width;
			
			// We only need to watch for orientation changes in landscape mode
			orientation = UIDeviceOrientationLandscapeLeft;
			[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
			[[NSNotificationCenter defaultCenter] addObserver:self
				selector:@selector(orientationChange:)
				name:@"UIDeviceOrientationDidChangeNotification" object:nil];
		}
		else {
			orientation = UIDeviceOrientationPortrait;
		}
		
		
		glview = [[EAGLView alloc] initWithFrame:CGRectMake(viewX, viewY, viewWidth*scale, viewHeight*scale)];
		[glview setContext:CanvasGlobalGLContext];
		[glview setFramebuffer];

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrthof(0, viewWidth*scale, viewHeight*scale, 0, -1, 1);
		
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glScalef(scale, scale, 1);
		if( landscapeMode ) {
			glRotatef( 90, 0, 0, 1 );
			glTranslatef(0, -height, 0);
		}
		
		glDisable(GL_CULL_FACE);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_TEXTURE_2D);
		
		[[Impact instance].view addSubview:glview];
	}
	return self;
}


- (void)dealloc {
	[glview removeFromSuperview];
	[glview release];
	[super dealloc];
}


- (void)setFrameBuffer {
	[glview setFramebuffer];
}


- (void)prepareThisCanvas {
	[self setFrameBuffer];

	glLoadIdentity();
	glScalef(scale, scale, 1);
	
	if( orientation == UIDeviceOrientationLandscapeLeft ) {
		glRotatef( 90, 0, 0, 1 );
		glTranslatef(0, -height, 0);
	}
	else if( orientation == UIDeviceOrientationLandscapeRight ) {
		glRotatef( -90, 0, 0, 1 );
		glTranslatef( -width, 0, 0);
	}
}


- (void)orientationChange:(NSNotification *)notification {	
	UIDeviceOrientation newOrientation = [[UIDevice currentDevice] orientation];
	if( 
		newOrientation != orientation &&
		UIDeviceOrientationIsLandscape(newOrientation) 
	) {
		orientation = newOrientation;
        
        UIInterfaceOrientation interfaceOrientation;
        interfaceOrientation = ((orientation == UIDeviceOrientationLandscapeLeft) ?
                                UIInterfaceOrientationLandscapeRight : UIInterfaceOrientationLandscapeLeft);
        
        [UIApplication sharedApplication].statusBarOrientation = interfaceOrientation;
        
		if( CanvasCurrentInstance == self ) {
			[self prepareThisCanvas];
		}
	}
}


JS_FUNC(JS_ScreenCanvas, present, ctx, argc, argv) {
	[self flushBuffers];
	[glview presentFramebuffer];
	drawCalls = 0;
	return NULL;
}

@end
