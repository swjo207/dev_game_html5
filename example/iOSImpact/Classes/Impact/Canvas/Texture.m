#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGLDrawable.h>
#import "Texture.h"


@implementation Texture
@synthesize textureId;
@synthesize width;
@synthesize height;
@synthesize realWidth;
@synthesize realHeight;

- (id)initWithPath:(NSString *)path {
	if( self = [super init] ) {		
		GLubyte * pixels = [self loadImageFromPath:path];
		
		bool wasEnabled = glIsEnabled(GL_TEXTURE_2D);
		int boundTexture = 0;
		glGetIntegerv(GL_TEXTURE_BINDING_2D, &boundTexture);
		
		glEnable(GL_TEXTURE_2D);
		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, realWidth, realHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		
		glBindTexture(GL_TEXTURE_2D, boundTexture);
		if( !wasEnabled ) {
			glDisable(GL_TEXTURE_2D);
		}
		
		free(pixels);
	}

	return self;
}


- (GLubyte *)loadImageFromPath:(NSString *)path {
	UIImage * tmpImage = [[UIImage alloc] initWithContentsOfFile:path];
	CGImageRef image = tmpImage.CGImage;
	
	width = CGImageGetWidth(image);
	height = CGImageGetHeight(image);
	
	realWidth = pow(2, ceil(log2( width )));
	realHeight = pow(2, ceil(log2( height )));
	
	GLubyte * pixels = (GLubyte *) malloc( realWidth * realHeight * 4);
	memset( pixels, 0, realWidth * realHeight * 4 );
	CGContextRef context = CGBitmapContextCreate(pixels, realWidth, realHeight, 8, realWidth * 4, CGImageGetColorSpace(image), kCGImageAlphaPremultipliedLast);
	CGContextDrawImage(context, CGRectMake(0.0, realHeight - height, (CGFloat)width, (CGFloat)height), image);
	CGContextRelease(context);
	
	[tmpImage release];
	return pixels;	
}


- (id)initWithWidth:(int)widthp height:(int)heightp {
	if( self = [super init] ) {
		width = widthp;
		height = heightp;
		
		realWidth = pow(2, ceil(log2( width )));
		realHeight = pow(2, ceil(log2( height )));
		
		bool wasEnabled = glIsEnabled(GL_TEXTURE_2D);
		int boundTexture = 0;
		glGetIntegerv(GL_TEXTURE_BINDING_2D, &boundTexture);
		
		glEnable(GL_TEXTURE_2D);
		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, realWidth, realHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		
		glBindTexture(GL_TEXTURE_2D, boundTexture);
		if( !wasEnabled ) {
			glDisable(GL_TEXTURE_2D);
		}
	}
	return self;
}


- (void)bind {
	glBindTexture(GL_TEXTURE_2D, textureId);
}


- (void)dealloc {
	glDeleteTextures( 1, &textureId );
	[super dealloc];
}

@end
