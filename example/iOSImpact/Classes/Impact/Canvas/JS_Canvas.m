#import "JS_Canvas.h"


gl_color JSValueToColor(JSContextRef ctx, JSValueRef value) {
	gl_color c = {.hex = 0xffffffff};
	
	if( !JSValueIsString(ctx, value) ) {
		return c;
	}
	
	JSStringRef jsString = JSValueToStringCopy( ctx, value, NULL );
	int length = JSStringGetLength( jsString );
	
	const JSChar * jsc = JSStringGetCharactersPtr(jsString);
	char str[] = "ffffff";
	
	// #f0f format
	if( length == 4 ) {
		str[0] = str[1] = jsc[3];
		str[2] = str[3] = jsc[2];
		str[4] = str[5] = jsc[1];
		c.hex = 0xff000000 | strtol( str, NULL, 16 );
	}
	
	// #ff00ff format
	else if( length == 7 ) {
		str[0] = jsc[5];
		str[1] = jsc[6];
		str[2] = jsc[3];
		str[3] = jsc[4];
		str[4] = jsc[1];
		str[5] = jsc[2];
		c.hex = 0xff000000 | strtol( str, NULL, 16 );
	}
	
	// rgb(255,0,255) format
	else { 
		GLubyte components[3] = {0,0,0};
		int current = 0;
		for( int i = 0; i < length && current < 3; i++ ) {
			if( isdigit(jsc[i]) ) {
				components[current] = components[current] * 10 + jsc[i] - '0'; 
			}
			else if( jsc[i] == ',' || jsc[i] == ')' ) {
				current++;
			}
		}
		c.rgba.r = components[0];
		c.rgba.g = components[1];
		c.rgba.b = components[2];
	}
	JSStringRelease(jsString);
	return c;
}





@implementation JS_Canvas

GLuint CanvasGlobalTextureFrameBuffer = 0;
EAGLContext * CanvasGlobalGLContext = nil;
JS_Canvas * CanvasCurrentInstance = NULL;

@synthesize texture;

- (id)initWithContext:(JSContextRef)ctx object:(JSObjectRef)obj argc:(size_t)argc argv:(const JSValueRef [])argv {
	if( self  = [super initWithContext:ctx object:obj argc:argc argv:argv] ) {
		scale = 1;
		width = 128;
		height = 128;
		
		globalAlpha = 255;
		fillColor.hex = 0xffffffff;
		clearColor[0] = 0;
		clearColor[1] = 0;
		clearColor[2] = 0;
		clearColor[3] = 1;
		
		// Create the global GLContext if we don't have one already
		if( !CanvasGlobalGLContext ) {
			CanvasGlobalGLContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
			if (!CanvasGlobalGLContext) {
				NSLog(@"GLDraw: Failed to create ES context");
			}
			else if (![EAGLContext setCurrentContext:CanvasGlobalGLContext]) {
				NSLog(@"GLDraw: Failed to set ES context current");
			}
		}		
	}
	return self;
}


- (void)dealloc {
	if( texture ) [texture release];
	[super dealloc];
}


- (void)switchToThisCanvas {
	[CanvasCurrentInstance flushBuffers];
	
	[self prepareThisCanvas];
	currentTexture = nil;
	CanvasCurrentInstance = self;
	
	quadIndex = 0;
	drawCalls = 0;
	
	glDisable(GL_TEXTURE_2D);
	glVertexPointer(2, GL_FLOAT, 0, quadVertices);
	glTexCoordPointer(2, GL_FLOAT, 0, textureVertices);
	glColorPointer(4, GL_UNSIGNED_BYTE, 0, vertexColors);
	
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
}




// -------------------------------------------------------------------------------------
// Setup

- (void)setFrameBuffer {
	[EAGLContext setCurrentContext:CanvasGlobalGLContext];
	
	// Create a global texture framebuffer
	if( !CanvasGlobalTextureFrameBuffer ) {
		glGenFramebuffersOES(1, &CanvasGlobalTextureFrameBuffer);
	}
	
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, CanvasGlobalTextureFrameBuffer);
}


- (void)prepareThisCanvas {
	if( !texture ) {
		[CanvasCurrentInstance flushBuffers];
		texture = [[Texture alloc] initWithWidth:width height:height];
	}
	[self setFrameBuffer];
	
	glFramebufferTexture2DOES(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_TEXTURE_2D, texture.textureId, 0);
	
	GLint framebufferHeight;
	glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &framebufferHeight);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0, framebufferHeight, 0);
	glScalef( 1, -1, 1 );
	
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
}




// -------------------------------------------------------------------------------------
// Drawing


- (void)flushBuffers {
	if( quadIndex == 0 ) {
		return;
	}
	
	glDrawArrays(GL_TRIANGLES, 0, 6 * quadIndex);
	
	quadIndex = 0;
	drawCalls++;
}


- (void)setCurrentTextureFromDrawable:(NSObject<Drawable> *)drawable {
	if( CanvasCurrentInstance != self ) {
		[self switchToThisCanvas];
	}
	
	if( !drawable && currentTexture ) {
		[self flushBuffers];
		glDisable(GL_TEXTURE_2D);
		currentTexture = nil;
	}
	
	Texture * newTexture = drawable.texture;
	if( newTexture != currentTexture ) {
		[self flushBuffers];
		glEnable(GL_TEXTURE_2D);
		currentTexture = newTexture;
		[currentTexture bind];
	}
}


- (void)drawQuadWithColor:(GLuint)color
	sx:(float)sx sy:(float)sy sw:(float)sw sh:(float)sh
	dx:(float)dx dy:(float)dy dw:(float)dw dh:(float)dh
	flipX:(BOOL)flipX flipY:(float)flipY
{	
	// first draw?
	if( drawCalls == 0 && quadIndex == 0 ) {
		[self setFrameBuffer];
	}
	else if( quadIndex >= CANVAS_CONTEXT_BUFFER_SIZE ) {
		[self flushBuffers];
	}
	
	
	GLfloat * qv = quadVertices[quadIndex];
	GLfloat * tv = textureVertices[quadIndex];
	GLuint * cv = (GLuint *)vertexColors[quadIndex];
	
	if( scale != 1 ) {
		dx = round(dx*scale)/scale;
		dy = round(dy*scale)/scale;
	}
	
	qv[8] = qv[4] = qv[0] = dx;
	qv[7] = qv[3] = qv[1] = dy;
	qv[10] = qv[6] = qv[2] = dx + dw;
	qv[11] = qv[9] = qv[5] = dy + dh;
	
	// do we have a texture?
	if( currentTexture ) {
		float tw = currentTexture.realWidth;
		float th = currentTexture.realHeight;
	
		tv[8] = tv[4] = tv[0] = flipX ? (sx + sw) / tw : sx / tw;
		tv[7] = tv[3] = tv[1] = flipY ? (sy + sh) / th : sy / th;
		tv[10] = tv[6] = tv[2] = flipX ? sx / tw : (sx + sw) / tw;
		tv[11] = tv[9] = tv[5] = flipY ? sy / th : (sy + sh) / th;
	}
	
	cv[0] = cv[1] = cv[2] = cv[3] = cv[4] = cv[5] = color;
	quadIndex++;
}


JS_FUNC(JS_Canvas, drawImageTile, ctx, argc, argv) {
	if( argc < 3 || !JSValueIsObject(ctx, argv[0]) ) return NULL;
	[self setCurrentTextureFromDrawable:(NSObject<Drawable> *)JSObjectGetPrivate((JSObjectRef)argv[0])];
	
	float sx, sy, sw, sh, dx, dy, dw, dh;
	gl_color color = {.rgba.r=255, .rgba.g=255, .rgba.b=255, .rgba.a=globalAlpha};
	BOOL flipX = NO, flipY = NO;
	
	if( argc > 7 ) {
		dx = JSValueToNumberFast(ctx, argv[1]);
		dy = JSValueToNumberFast(ctx, argv[2]);
		int t = JSValueToNumberFast(ctx, argv[3]);
		
		dw = sw = JSValueToNumberFast(ctx, argv[4]);
		dh = sh = JSValueToNumberFast(ctx, argv[5]);
		
		sx = (int)(t * sw) % (int)currentTexture.width;
		sy = (int)(t * sw / currentTexture.width) * sh;
		
		flipX = JSValueToBoolean(ctx, argv[6]);
		flipY = JSValueToBoolean(ctx, argv[7]);
		if( argc > 8 ) {
			color.rgba.a = MIN(255,MAX(255 * JSValueToNumberFast(ctx, argv[8]),0));
		}
	}
	else {
		dx = JSValueToNumberFast(ctx, argv[1]);
		dy = JSValueToNumberFast(ctx, argv[2]);
		color.rgba.a = MIN(255,MAX(255 * JSValueToNumberFast(ctx, argv[3]),0));
		sx = sy = 0;
		sw = dw = currentTexture.width;
		sh = dh = currentTexture.height;
	}
	
	[self drawQuadWithColor:color.hex
		sx:sx sy:sy sw:sw sh:sh
		dx:dx dy:dy dw:dw dh:dh
		flipX:flipX flipY:flipY];
	
	return NULL;
}


JS_FUNC(JS_Canvas, drawImage, ctx, argc, argv) {
	if( argc < 3 || !JSValueIsObject(ctx, argv[0]) ) return NULL;
	[self setCurrentTextureFromDrawable:(NSObject<Drawable> *)JSObjectGetPrivate((JSObjectRef)argv[0])];
	
	float
		dx = JSValueToNumberFast(ctx, argv[1]),
		dy = JSValueToNumberFast(ctx, argv[2]),
		w = currentTexture.width,
		h = currentTexture.height;
	
	gl_color color = {.rgba.r=255, .rgba.g=255, .rgba.b=255, .rgba.a=globalAlpha};
	[self drawQuadWithColor:color.hex
		sx:0 sy:0 sw:w sh:h
		dx:dx dy:dy dw:w dh:h
		flipX:NO flipY:NO];

	return NULL;
}


JS_FUNC(JS_Canvas, drawFont, ctx, argc, argv) {
	if( argc < 5 || !JSValueIsObject(ctx, argv[0]) ) return NULL;
	[self setCurrentTextureFromDrawable:(NSObject<Drawable> *)JSObjectGetPrivate((JSObjectRef)argv[0])];
	
	Font * font = (Font *)currentTexture;
	
	
	float w, h, dx, dy;
	NSString * text = JSValueToNSString(ctx, argv[1]);
	int length = text.length;
	
	dx = JSValueToNumberFast(ctx, argv[2]) 
		+ [font offsetForText:text withAlignment:JSValueToNumberFast(ctx, argv[4])];
	dy = JSValueToNumberFast(ctx, argv[3]);
	
	h = font.height;
	gl_color color = {.rgba.r=255, .rgba.g=255, .rgba.b=255, .rgba.a=globalAlpha};
	
	for( int i = 0; i < length; i++ ) {
		unichar c = [text characterAtIndex:i];
		
		float x, y;
		[font indexForChar:c x:&x y:&y];
		w = [font widthForChar:c];
		
		[self drawQuadWithColor:color.hex
			sx:x sy:y sw:w sh:h
			dx:dx dy:dy dw:w dh:h
			flipX:NO flipY:NO];
			
		dx += w + FONT_CHAR_SPACING;
	}
	
	return NULL;
}


JS_FUNC(JS_Canvas, save, ctx, argc, argv) {
	[self flushBuffers];
	glPushMatrix();
	return NULL;
}


JS_FUNC(JS_Canvas, restore, ctx, argc, argv) {
	[self flushBuffers];
	glPopMatrix();
	return NULL;
}


JS_FUNC(JS_Canvas, rotate, ctx, argc, argv) {
	[self flushBuffers];
	glRotatef(JSValueToNumberFast(ctx, argv[0]) * 180 / M_PI, 0, 0, 1);
	return NULL;
}


JS_FUNC(JS_Canvas, translate, ctx, argc, argv) {
	[self flushBuffers];
	glTranslatef(JSValueToNumberFast(ctx, argv[0]), JSValueToNumberFast(ctx, argv[1]), 0);
	return NULL;
}


JS_FUNC(JS_Canvas, scale, ctx, argc, argv) {
	[self flushBuffers];
	glScalef(JSValueToNumberFast(ctx, argv[0]), JSValueToNumberFast(ctx, argv[1]), 1);
	return NULL;
}


JS_FUNC(JS_Canvas, clear, ctx, argc, argv) {
	glClearColor( clearColor[0], clearColor[1], clearColor[2], clearColor[3] );
    glClear(GL_COLOR_BUFFER_BIT);
	return NULL;
}


JS_FUNC(JS_Canvas, fillRect, ctx, argc, argv) {
	[self setCurrentTextureFromDrawable:nil];
	
	float
		dx = JSValueToNumberFast(ctx, argv[0]),
		dy = JSValueToNumberFast(ctx, argv[1]),
		w = JSValueToNumberFast(ctx, argv[2]),
		h = JSValueToNumberFast(ctx, argv[3]);
	
	fillColor.rgba.a = globalAlpha;
	
	[self drawQuadWithColor:fillColor.hex
		sx:0 sy:0 sw:w sh:h
		dx:dx dy:dy dw:w dh:h
		flipX:NO flipY:NO];
	
	return NULL;
}

JS_FUNC(JS_Canvas, strokeRect, ctx, argc, argv) {
	return NULL; // FIXME: stub
}

JS_GET(JS_Canvas, fillStyle, ctx ) {
	return JSValueMakeNumber(ctx, 0 ); // FIXME: stub
}

JS_SET(JS_Canvas, fillStyle, ctx, value) {
	fillColor = JSValueToColor( ctx, value );
}

JS_GET(JS_Canvas, clearStyle, ctx ) {
	return JSValueMakeNumber(ctx, 0 ); // FIXME: stub
}

JS_SET(JS_Canvas, clearStyle, ctx, value) {
	gl_color c = JSValueToColor( ctx, value );
	clearColor[0] = c.rgba.r / 255.0f;
	clearColor[1] = c.rgba.g / 255.0f;
	clearColor[2] = c.rgba.b / 255.0f;
	clearColor[3] = c.rgba.a / 255.0f;
}

JS_GET(JS_Canvas, globalAlpha, ctx ) {
	return JSValueMakeNumber(ctx, globalAlpha/255.0f );
}

JS_SET(JS_Canvas, globalAlpha, ctx, value) {
	globalAlpha = MIN(255,MAX(JSValueToNumberFast(ctx, value)*255,0));
}

JS_GET(JS_Canvas, globalCompositeOperation, ctx) {
	// FIXME: stub
	JSStringRef s = JSStringCreateWithUTF8CString("source-over");
	JSValueRef ret = JSValueMakeString(ctx, s);
	JSStringRelease(s);
	return ret;
}

JS_SET(JS_Canvas, globalCompositeOperation, ctx, value) {
	if( !JSValueIsString(ctx, value) ) return;
	
	[self flushBuffers];
	
	// Just check for the length of the string to determine the blend mode
	// This is a cheap hack... FIXME
	int length = JSStringGetLength( (JSStringRef)value ) + 1;
	if( length == sizeof("lighter") ) {
		glBlendFunc(GL_ONE, GL_ONE);
	}
	else if( length == sizeof("darker") ) {
		glBlendFunc(GL_ZERO, GL_SRC_COLOR);
	}
	else { // source-over
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
}

JS_FUNC(JS_Canvas, getContext, ctx, argc, argv) {
	// Context and canvas are one and the same object, so getContext just
	// returns itself
	return (JSValueRef)object;
}

JS_GET(JS_Canvas, width, ctx) {
	return JSValueMakeNumber(ctx, width);
}

JS_SET(JS_Canvas, width, ctx, value) {
	width = JSValueToNumberFast(ctx, value);
}

JS_GET(JS_Canvas, height, ctx) {
	return JSValueMakeNumber(ctx, height);
}

JS_SET(JS_Canvas, height, ctx, value) {
	height = JSValueToNumberFast(ctx, value);
}


@end
