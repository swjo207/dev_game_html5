#import <Foundation/Foundation.h>
#import "JavaScriptCore/JavaScriptCore.h"

#define IMPACT_GAME_FOLDER @"game/"

#define IMPACT_DEBUG_BOOT_JS @"ios-impact.js"
#define IMPACT_DEBUG_MAIN_JS @"index.js"
#define IMPACT_RELEASE_MASTER_JS @"game.min.js"

NSString * JSValueToNSString( JSContextRef ctx, JSValueRef v );
double JSValueToNumberFast( JSContextRef ctx, JSValueRef v );

@protocol TouchDelegate
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
@end


@interface Impact : UIViewController {
	JSGlobalContextRef ctx;
	UIWindow * window;
	NSMutableDictionary * jsClasses;
	UIImageView * loadingScreen;
	NSObject<TouchDelegate> * touchDelegate;
}

- (id)initWithWindow:(UIWindow *)window;

- (JSClassRef)getJSClassForClass:(id)classId;
- (void)hideLoadingScreen;
- (void)loadScriptAtPath:(NSString *)path;
- (JSValueRef)invokeCallback:(JSObjectRef)callback thisObject:(JSObjectRef)thisObject argc:(size_t)argc argv:(const JSValueRef [])argv;
- (void)logException:(JSValueRef)exception ctx:(JSContextRef)ctxp;


+ (Impact *)instance;
+ (NSString *)pathForResource:(NSString *)resourcePath;
+ (BOOL)landscapeMode;
+ (BOOL)statusBarHidden;

@property (readonly) JSGlobalContextRef ctx;
@property (readonly) UIWindow * window;
@property (nonatomic,retain) NSObject<TouchDelegate> * touchDelegate;

@end
