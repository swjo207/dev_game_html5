#import "Impact.h"
#import "JS_BaseClass.h"
#import <objc/runtime.h>

JSClassRef impact_constructorClass;


NSString * JSValueToNSString( JSContextRef ctx, JSValueRef v ) {
	JSStringRef jsString = JSValueToStringCopy( ctx, v, NULL );
	if( !jsString ) return nil;
	
	NSString * string = (NSString *)JSStringCopyCFString( kCFAllocatorDefault, jsString );
	[string autorelease];
	JSStringRelease( jsString );
	
	return string;
}

double JSValueToNumberFast( JSContextRef ctx, JSValueRef v ) {
	unsigned char * bytes = ((unsigned char *) v) + 8;
	unsigned char * tagBytes = ((unsigned char *) v) + 12;
	int32_t unionTag = *((int32_t*)tagBytes);
	if( unionTag < 0xfffffff8 ) {
		return *((double *) bytes);
	}
	else {
		return *((int32_t *) bytes);
	}
}

JSValueRef impact_getNativeClass(JSContextRef ctx, JSObjectRef object, JSStringRef propertyNameJS, JSValueRef* exception) {
	CFStringRef className = JSStringCopyCFString( kCFAllocatorDefault, propertyNameJS );
	
	JSObjectRef obj = NULL;
	NSString * fullClassName = [NSString stringWithFormat:@"JS_%@", className];
	id class = NSClassFromString(fullClassName);
	if( class ) {
		obj = JSObjectMake( ctx, impact_constructorClass, (void *)class );
	}
	
	CFRelease(className);
	return obj ? obj : JSValueMakeUndefined(ctx);
}

JSObjectRef impact_callAsConstructor(JSContextRef ctx, JSObjectRef constructor, size_t argc, const JSValueRef argv[], JSValueRef* exception) {
	id class = (id)JSObjectGetPrivate( constructor );
	
	JSClassRef jsClass = [[Impact instance] getJSClassForClass:class];
	JSObjectRef obj = JSObjectMake( ctx, jsClass, NULL );
	
	id instance = [(JS_BaseClass *)[class alloc] initWithContext:ctx object:obj argc:argc argv:argv];
	JSObjectSetPrivate( obj, (void *)instance );
	
	return obj;
}




@implementation Impact
@synthesize ctx;
@synthesize window;
@synthesize touchDelegate;

static Impact * impactInstance = NULL;

+ (Impact *)instance {
	return impactInstance;
}


- (id)initWithWindow:(UIWindow *)windowp {
	if( self = [super init] ) {
	
		impactInstance = self;
		window = windowp;
		[window addSubview:self.view];
		
		// Show the loading screen
		loadingScreen = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default.png"]];
		[self.view addSubview:loadingScreen];
		
				
		// Create the global JS context and attach the 'native' object
		jsClasses = [[NSMutableDictionary alloc] init];
		
		JSClassDefinition constructorClassDef = kJSClassDefinitionEmpty;
		constructorClassDef.callAsConstructor = impact_callAsConstructor;
		impact_constructorClass = JSClassCreate(&constructorClassDef);
		
		JSClassDefinition globalClassDef = kJSClassDefinitionEmpty;
		globalClassDef.getProperty = impact_getNativeClass;		
		JSClassRef globalClass = JSClassCreate(&globalClassDef);
		
		
		ctx = JSGlobalContextCreate(NULL);
		JSObjectRef globalObject = JSContextGetGlobalObject(ctx);
		
		JSObjectRef iosObject = JSObjectMake( ctx, globalClass, NULL );
		JSObjectSetProperty(
			ctx, globalObject, 
			JSStringCreateWithUTF8CString("native"), iosObject, 
			kJSPropertyAttributeDontDelete | kJSPropertyAttributeReadOnly, NULL
		);
		
		// Load the BOOT and MAIN js for Debug builds or the baked
		// MASTER js for release builds
		#ifdef DEBUG
			[self loadScriptAtPath:IMPACT_DEBUG_BOOT_JS];
			[self loadScriptAtPath:IMPACT_DEBUG_MAIN_JS];
		#else
			[self loadScriptAtPath:IMPACT_RELEASE_MASTER_JS];
		#endif
	}
	return self;
}


- (void)hideLoadingScreen {
	[loadingScreen removeFromSuperview];
	[loadingScreen release];
}


- (JSClassRef)getJSClassForClass:(id)classId {
	JSClassRef jsClass = [[jsClasses objectForKey:classId] pointerValue];
	
	// Not already loaded? Ask the objc class for the JSClassRef!
	if( !jsClass ) {
		jsClass = [classId getJSClass];
		[jsClasses setObject:[NSValue valueWithPointer:jsClass] forKey:classId];
	}
	return jsClass;
}


- (void)loadScriptAtPath:(NSString *)path {
	NSString * script = [NSString stringWithContentsOfFile:[Impact pathForResource:path] encoding:NSUTF8StringEncoding error:NULL];	
	
	if( !script ) {
		NSLog(@"Can't load Script: %@", path );
		return;
	}
	
	NSLog(@"Loading Script: %@", path );
	JSStringRef scriptJS = JSStringCreateWithCFString((CFStringRef)script);
	JSStringRef pathJS = JSStringCreateWithCFString((CFStringRef)path);
	
	JSValueRef exception = NULL;
	JSEvaluateScript( ctx, scriptJS, NULL, pathJS, 0, &exception );
	[self logException:exception ctx:ctx];

	JSStringRelease( scriptJS );
}


- (JSValueRef)invokeCallback:(JSObjectRef)callback thisObject:(JSObjectRef)thisObject argc:(size_t)argc argv:(const JSValueRef [])argv {
	JSValueRef exception = NULL;
	JSValueRef result = JSObjectCallAsFunction( ctx, callback, thisObject, argc, argv, &exception );
	[self logException:exception ctx:ctx];
	return result;
}


- (void)logException:(JSValueRef)exception ctx:(JSContextRef)ctxp {
	if( !exception ) return;
	
	JSStringRef jsLinePropertyName = JSStringCreateWithUTF8CString("line");
	JSStringRef jsFilePropertyName = JSStringCreateWithUTF8CString("sourceURL");
	
	JSObjectRef exObject = JSValueToObject( ctxp, exception, NULL );
	JSValueRef line = JSObjectGetProperty( ctxp, exObject, jsLinePropertyName, NULL );
	JSValueRef file = JSObjectGetProperty( ctxp, exObject, jsFilePropertyName, NULL );
	
	NSLog( 
		@"%@ at line %@ in %@", 
		JSValueToNSString( ctxp, exception ),
		JSValueToNSString( ctxp, line ),
		JSValueToNSString( ctxp, file )
	);
	
	JSStringRelease( jsLinePropertyName );
	JSStringRelease( jsFilePropertyName );
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	if( touchDelegate ) {
		[touchDelegate touchesBegan:touches withEvent:event];
	}
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	if( touchDelegate ) {
		[touchDelegate touchesEnded:touches withEvent:event];
	}
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	if( touchDelegate ) {
		[touchDelegate touchesMoved:touches withEvent:event];
	}
}


- (void)dealloc {
	JSGlobalContextRelease(ctx);
	[touchDelegate release];
	[jsClasses release];
	[super dealloc];
}



+ (NSString *)pathForResource:(NSString *)path {
	return [NSString stringWithFormat:@"%@/" IMPACT_GAME_FOLDER "%@", [[NSBundle mainBundle] resourcePath], path];
}

+ (BOOL)landscapeMode {
	return [[[[NSBundle mainBundle] infoDictionary] 
		objectForKey:@"UIInterfaceOrientation"] hasPrefix:@"UIInterfaceOrientationLandscape"];
	
}

+ (BOOL)statusBarHidden {
	return [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"UIStatusBarHidden"] boolValue];
}

@end
