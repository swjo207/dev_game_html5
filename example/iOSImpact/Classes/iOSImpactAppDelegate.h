#import <UIKit/UIKit.h>
#import "Impact.h"

@interface iOSImpactAppDelegate : NSObject <UIApplicationDelegate> {
	Impact * engine;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@end

